package examenev03_MiguelPerú;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.cuatrovientos.examenfinal.Coche;
import org.cuatrovientos.examenfinal.GestorAlquiler;
import org.junit.jupiter.api.Test;

class GestorAlquilerTest {

	private GestorAlquiler target;
	Coche c = new Coche("ABC", "1234CC", 5, false);

	@Test
	void testToString() {
		String expected = "Coche: Modelo: ABC, Matricula: 1234CC, Plazas: 5, Alquilado: No";
		String actual = target.toString();
		assertEquals(expected, actual);
	}

	@Test
	void testCochesPorPlazas() {
		int expected = 1;
		int actual = target.cochesPorPlazas(5);
		assertEquals(expected, actual);
	}

	@Test
	void testMotosDeMarca() {
		int expected = 0;
		int actual = target.motosDeMarca("Nissan");
		assertEquals(expected, actual);
	}

}
