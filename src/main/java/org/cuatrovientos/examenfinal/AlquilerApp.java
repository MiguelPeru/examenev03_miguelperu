package org.cuatrovientos.examenfinal;

public class AlquilerApp {

	public static void main(String[] args) {
		GestorAlquiler gestor = new GestorAlquiler();
		
		gestor.llenarMapa();
		System.out.println("Coches Por Plazas Valor Incorrecto");
		gestor.cochesPorPlazas(-1);
		System.out.println("\nCoches Por Plazas Valor Correcto");
		gestor.cochesPorPlazas(7);
		
		System.out.println("\n\nVehiculos Ordenados");
		gestor.ordenarVehiculos();
		System.out.println(gestor.toString());
		
		System.out.println("Motos De Marca Valor Incorrecto");
		gestor.motosDeMarca("KTM");
		System.out.println("\nMotos De Marca Valor Correcto");
		gestor.motosDeMarca("BMW");
		
		System.out.println("\n\nGuardar Estadisticas -->Ver Archivo");
		gestor.guardarEstadisticas();
		
	}
}