package org.cuatrovientos.examenfinal;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class GestorAlquiler {

	HashMap<String, ArrayList<Vehiculo>> mapaVehiculos;

	/***
	 * Constructor que inicializa el mapa
	 */
	public GestorAlquiler() {
		this.mapaVehiculos = new HashMap<String, ArrayList<Vehiculo>>();
	}
	
	/***
	 * A�ade un vehiculo a la marca correspondiente solo en el caso de que el vehiculo
	 * no estuviese ya introducido, si la marcca no existiese se a�ade tanto la marca 
	 * como el vehiculo.
	 * @param marca
	 * @param vehiculo
	 */

	public void anadirVehiculo(String marca, Vehiculo vehiculo) {
		if (!mapaVehiculos.containsKey(marca)) {
			ArrayList<Vehiculo> vehiculos = new ArrayList<Vehiculo>();
			vehiculos.add(vehiculo);
			this.mapaVehiculos.put(marca, vehiculos);
		} else {
			List<Vehiculo> personajes = this.mapaVehiculos.get(marca);
			if (!personajes.contains(vehiculo)) {
				personajes.add(vehiculo);
			}
		}
	}
	
	/***
	 * Llena el mapa con el contenido del archivo llamado vehiculos situado en la
	 * carpeta src. La estructura del archivo es la siguiente:
	 * Coches
	 * C:Marca:Modelo:Matricula:Plazas
	 * Motos
	 * M:Marca:Modelo:Matricula:Plazas:CarnetNecesario
	 */
	public void llenarMapa() {
		Scanner entrada = null;
		try {
			entrada = new Scanner(new File("src/main/resources/vehiculos"));
			while (entrada.hasNextLine()) {
				String linea = entrada.nextLine();

				String[] vehiculo = linea.split(":");
				String marca = vehiculo[1];
				String modelo = vehiculo[2];
				String matricula = vehiculo[3];
				int plazas = Integer.parseInt(vehiculo[4]);

				boolean alquilado;

				if (vehiculo[5].equals("SI")) {
					alquilado = true;
				} else {
					alquilado = false;
				}

				if (vehiculo[0].equals("C")) {

					Coche coche = new Coche(modelo, matricula, plazas, alquilado);
					anadirVehiculo(marca, coche);
				} else {
					boolean carnet;
					if (vehiculo[6].equals("SI")) {
						carnet = true;
					} else {
						carnet = false;
					}
					Moto moto = new Moto(modelo, matricula, plazas, alquilado, carnet);
					anadirVehiculo(marca, moto);
				}
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		finally {
			try {
				entrada.close();
			} catch (NullPointerException e) {
				System.out.println("Error en IO , no se ha creado el fichero");
			}
		}
	}
	
	/***
	 * Representaci�n textual del mapa tal y como se muestra en los resultados
	 * de ejecuci�n
	 * @return Mapa de los veh�culos.
	 */
	public String toString() {
		Set<String> claves = this.mapaVehiculos.keySet();
		String mapa = "";

		for (String marca : claves) {
			ArrayList<Vehiculo> tmp = this.mapaVehiculos.get(marca);

			mapa += "\n" + marca;
			mapa += "\n==========";

			for (Vehiculo vehiculo : tmp) {
				mapa += vehiculo.toString() + "\n";
			}

		}

		return mapa;
	}
	
	/***
	 * Muestra los coches que tengan al menos las plazas pasadas por par�metro.
	 * Se debe comprobar que las plazas pasadas por par�metro son mayores a 0.
	 * @param plazas
	 * @return Integer, n�mero de coches por plaza.
	 */
	public int cochesPorPlazas(int plazas) {
		int cntPlazas=0;
		if (plazas > 0) {

			Set<String> claves = this.mapaVehiculos.keySet();
			ArrayList<Vehiculo> listaVehiculos;

			for (String marca : claves) {
				listaVehiculos = this.mapaVehiculos.get(marca);
				for (Vehiculo vehiculo : listaVehiculos) {

					if (vehiculo instanceof Coche) {
						if (vehiculo.getPlazas() >= plazas) {
							// System.out.print(vehiculo.toString());
							cntPlazas++;
						}
					}
				}

			}
		} else {
			System.out.print("El n�mero de plazas es incorrecto");
		}
		return cntPlazas;

	}
	
	/***
	 * Ordena los vehiculos de cada marca utilizando el m�todo de ordenaci�n definido
	 * para la clase Vehiculo
	 */
	public void ordenarVehiculos() {
		Set<String> claves = this.mapaVehiculos.keySet();
		ArrayList<Vehiculo> listaVehiculos;

		for (String marca : claves) {
			listaVehiculos = this.mapaVehiculos.get(marca);
			Collections.sort(listaVehiculos);
		}
	}

	/***
	 * Muestra las motos de la marca pasada por par�metro que no esten alquiladas. Si la marca 
	 * no existe, se devolver� un mensaje de error
	 * @param marca
	 * @return Integer, cantidad de motos de esa marca.
	 */
	public int motosDeMarca(String marca) {
		int cntMotos = 0;
		if (this.mapaVehiculos.containsKey(marca)) {

			ArrayList<Vehiculo> listaVehiculos = this.mapaVehiculos.get(marca);
			for (Vehiculo vehiculo : listaVehiculos) {

				if (vehiculo instanceof Moto) {
					if (!vehiculo.isAlquilado())
						//System.out.print(vehiculo.toString());
						cntMotos++;
				}
			}
		} else {
			System.out.print("La marca introducida es incorrecta");
		}
		return cntMotos;

	}
/***
 * Debe guardar en el fichero estadisticas situado en la carpeta src el numero de motos y coches
 * que hay disponibles(no alguilados) por cada marca. Ver resultados de ejecuci�n
 */
	public void guardarEstadisticas() {
		PrintWriter fsalida = null;
		int coches;
		int motos;
		try {
			fsalida = new PrintWriter(new BufferedWriter(new FileWriter("src/main/resources/estadisticas")));

			Set<String> claves = this.mapaVehiculos.keySet();

			for (String marca : claves) {
				coches =0;
				motos = 0;
				
				ArrayList<Vehiculo> listaVehiculos = this.mapaVehiculos.get(marca);
				for (Vehiculo vehiculo : listaVehiculos) {
					if (!vehiculo.isAlquilado()) {
						if (vehiculo instanceof Coche) {
							coches++;
						}
						else {
							motos++;
						}
					}
				}
				if (coches>0) {
					fsalida.println("Hay " + coches + " coches disponibles de la marca " + marca);
				}
				if (motos>0) {
					fsalida.println("Hay " + motos + " motos disponibles de la marca " + marca);
				}
			}

		} catch (IOException e) {
			System.out.println("Error al crear guardar los personajes");
		} finally {
			fsalida.close();
		}
	}

}
