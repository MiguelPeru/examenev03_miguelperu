package org.cuatrovientos.examenfinal;

public abstract class Vehiculo implements Comparable<Vehiculo>{
	
	private String modelo;
	private String matricula;
	private int plazas;
	private boolean alquilado;
	
	public Vehiculo (String modelo,String matricula, int plazas, boolean alquilado) {
		this.modelo=modelo;
		this.matricula=matricula;
		this.plazas=plazas;
		this.alquilado=alquilado;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public int getPlazas() {
		return plazas;
	}

	public void setPlazas(int plazas) {
		this.plazas = plazas;
	}

	public boolean isAlquilado() {
		return alquilado;
	}

	public void setAlquilado(boolean alquilado) {
		this.alquilado = alquilado;
	}

	
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (alquilado ? 1231 : 1237);
		result = prime * result + ((matricula == null) ? 0 : matricula.hashCode());
		result = prime * result + ((modelo == null) ? 0 : modelo.hashCode());
		result = prime * result + plazas;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vehiculo other = (Vehiculo) obj;
		if (alquilado != other.alquilado)
			return false;
		if (matricula == null) {
			if (other.matricula != null)
				return false;
		} else if (!matricula.equals(other.matricula))
			return false;
		if (modelo == null) {
			if (other.modelo != null)
				return false;
		} else if (!modelo.equals(other.modelo))
			return false;
		if (plazas != other.plazas)
			return false;
		return true;
	}

	public int compareTo(Vehiculo v) {
		if (this.modelo.equals(v.getModelo())) {
			if (this.alquilado && !v.isAlquilado()) {
				return 1;
			}	
			else if (!this.alquilado && v.isAlquilado()) {
				return -1;
			}
			else {
				return 0;
			}
		}
		else {
			return this.modelo.compareTo(v.getModelo());
		}
	}
	
	public String toString () {
		String vehiculo="";
		
		vehiculo+= this.getClass().getSimpleName();
		vehiculo+= ": Modelo: "+this.modelo;
		vehiculo+= ", Matricula: "+this.matricula;
		vehiculo+= ", Plazas: "+this.plazas;
		if (this.alquilado) {
			vehiculo+= ", Alquilado: Si";
		}
		else {
			vehiculo+= ", Alquilado: No";
		}
		
		return vehiculo;
	}
	
	
}
