package org.cuatrovientos.examenfinal;

public class Moto extends Vehiculo{
	
	boolean carnet;

	public Moto (String modelo, String matricula, int plazas, boolean alquilado, boolean carnet) {
		super(modelo,matricula,plazas,alquilado);
		this.carnet=carnet;
	}

	public boolean isCarnet() {
		return carnet;
	}

	public void setCarnet(boolean carnet) {
		this.carnet = carnet;
	}

	
	@Override
	public String toString() {
		String moto = super.toString();
		
		if (carnet) {
			moto+= "\nCarnet de moto: Si";
		}
		else {
			moto+= "\nCarnet de moto: No";
		}
		
		return moto;
	}
}
